import React, { Component } from "react";
import "./App.css";
import FeedPreview from "./components/FeedPreview";


class App extends Component {

  render() {
    return (
      <div className="App">
        <FeedPreview />
      </div>
    );
  }
}

export default App;
