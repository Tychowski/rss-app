import React, { Component } from "react";
import Input from "@material-ui/core/Input";
import Button from "@material-ui/core/Button";

class UserForm extends Component {
  constructor(props) {
    super(props);

    this.state = {
      enabled: true,
      feeds: []
    };

    this.handleSearchChange = this.handleSearchChange.bind(this);
  }

  handleSearchChange(e) {
    const value = e.target.value;
    if (value !== "") {
      this.setState({ enabled: false, feeds: [...this.state.feeds, value] });
    } else {
      this.setState({ enabled: true });
    }
  }

  render() {
    return (
      <div>
        <form onSubmit={this.props.getFeed}>
          <Input
            style={{ margin: "50px 20px", display: "block" }}
            type="text"
            name="feed_url"
            placeholder="Please enter an RSS feed"
            onChange={this.handleSearchChange}
          />
          <Button
            type="submit"
            variant="contained"
            color="primary"
            disabled={this.state.enabled}
          >
            Submit
          </Button>
        </form>
      </div>
    );
  }
}

export default UserForm;
