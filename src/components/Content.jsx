import React, { Component } from "react";

class Content extends Component {
  divStyles = {
    width: "77vw",
    float: "right",
    marginRight: "1vw",
    borderBottom: "1px solid #818181c7"
  };
  render() {
    return (
      <div className="list-group" style={this.divStyles}>
        <a
          href={this.props.link}
          className="list-group-item list-group-item-action text-left"
        >
          {this.props.title}
        </a>
      </div>
    );
  }
}

export default Content;
